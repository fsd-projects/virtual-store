const mongoose = require('mongoose');

const detailsSchema = new mongoose.Schema({
    name: String,
    address: String
  });
  
module.exports = mongoose.model('details', detailsSchema);

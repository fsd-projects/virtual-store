const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    id: String,
    name: String,
    description: String,
    price: Number,
    photo: String
  });
  
module.exports = mongoose.model('products', productSchema);

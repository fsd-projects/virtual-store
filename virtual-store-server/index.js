const express = require("express");
const cors = require('cors');
const {default: mongoose} = require("mongoose");
const app = express();
const bodyParser = require('body-parser');
const methodOverride = require('method-override');


const Product = require('./models/product');
const Detail = require('./models/details');

app.use(cors());
app.use(bodyParser.json());
app.use(methodOverride('_method'));
app.use(express.urlencoded({extended: true}));
app.set('view engine', 'ejs');

mongoose.connect("mongodb+srv://virtualshop:virtualshop1@cluster0.zyaexlb.mongodb.net/virtual-shop",
 { useNewUrlParser: true })
    .then(() => {
        console.log("connection started");
    }).catch(err => {
        console.log("connection error:" + err);
    });
   
app.get('/products', async (req, res) => {
    const products = await Product.find({});
    console.log(products);
    res.json({ products });
});

app.post('/details', async (req, res) => {
    const newDetails = new Detail(req.body);
    await newDetails.save();
    res.send();
});

const port = 3001;
app.listen(port, () => {
    console.log("listening on port " + port);
});
import styled from "styled-components";


export function ProductsList({products, cartProducts, setCartProducts, isCart = false}) {
    const addToCart = (product) => {
        cartProducts.push(product);
        setCartProducts(cartProducts);
    };
    
    return (<ShoppingList>
        {(isCart ? cartProducts : products).length>0  
        && (isCart ? cartProducts : products).map(product =>(
            <Item>
                <MainInfo>
                    <div>{product.name}</div>
                    <div>{product.price}</div>
                    <div>{product.description}</div>
                </MainInfo>
                <MainPicture>
                    <Image src={product.photo} alt="Avatar"/>
                </MainPicture>
                {!isCart && <div onClick={() => addToCart(product)}>add to cart</div>}
            </Item>)
        )}
    </ShoppingList>);
}


const Image = styled.img`
    height: 15vmin;
`;

const MainPicture = styled.div`
    margin: 1em 1em 1em 1em;
`;
const MainInfo = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    width: 100%;
    margin: 1em 0 1em 2em;
`;

const ShoppingList = styled.div`
    margin: 5em 2em 5em 2em;
`;

const Item = styled.div`
    margin: 5em 0 0 0;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    display: flex;
    
    &:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
`;

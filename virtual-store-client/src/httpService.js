import axios from "axios";

const serverUrl = 'http://localhost:3001/';

class HttpService {

    static async getProducts() {
        return await axios.get(serverUrl + "products");
    }

    static async addDetails(details) {
        return await axios.post(serverUrl + "details", details);
    }
}

export default HttpService;
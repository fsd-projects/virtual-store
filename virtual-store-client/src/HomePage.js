import { useState } from 'react';
import { useEffect } from 'react';
import styled from 'styled-components'
import HttpService from './httpService';
import {ProductsList} from "./ProductsList";
import {ShoppingCart} from "./ShoppingCart";


export function HomePage() {
    const [products, setProducts] = useState([]);
    const [cartProducts, setCartProducts] = useState([]);
    useEffect(() => {HttpService.getProducts().then((res) => setProducts(res.data.products))}, [false]);

    return (<Container>
        <ProductsList products={products} cartProducts={cartProducts} setCartProducts={setCartProducts}></ProductsList>
        <ShoppingCart cartProducts={cartProducts}></ShoppingCart>
    </Container>);
}

const Container = styled.div`
    display: flex;
`;

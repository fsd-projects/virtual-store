import {useState} from "react";
import styled from "styled-components";
import {ProductsList} from "./ProductsList";
import HttpService from "./httpService";

export function ShoppingCart({cartProducts}) {
    const [isOpen, setIsOpen] = useState(false);
    const [nameInput, setNameInput] = useState('');
    const [addressInput, setAddressInput] = useState('');

    const toggleShoppingCart = (isOpen) => {
        setIsOpen(!isOpen);
    };

    const handleSubmit = (event) => {
        var details = { name: nameInput, address: addressInput };
        HttpService.addDetails(details).then(
            alert('save to db:' + nameInput+addressInput));
        event.preventDefault();
    };

    return (
        <Cart>
            <div onClick={() => toggleShoppingCart(isOpen)}>Shopping Cart</div>
            {isOpen &&
                <>
                    <div>price: {cartProducts.map(product => product.price)?.reduce((a,b)=> a + b, 0)}</div>
                    <ProductsList cartProducts={cartProducts} products={cartProducts} isCart={true}></ProductsList>
                    <form onSubmit={handleSubmit}>
                        <label>
                            Name:
                            <input type="text" value={nameInput} onChange={(e) => setNameInput(e.target.value)} />
                        </label>
                        <label>
                            Address:
                            <input type="text" value={addressInput} onChange={(e) => setAddressInput(e.target.value)} />
                        </label>
                        <input type="submit" value="Submit" />
                    </form>
                </>
            }
        </Cart>
    );
}

const Cart = styled.div`
    height: 13em;
    margin: 5em 20em 5em 20em;
`;